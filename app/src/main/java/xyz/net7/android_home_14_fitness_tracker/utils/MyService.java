package xyz.net7.android_home_14_fitness_tracker.utils;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import xyz.net7.android_home_14_fitness_tracker.model.DataAcc;
import xyz.net7.android_home_14_fitness_tracker.model.DataUserActivity;

import static xyz.net7.android_home_14_fitness_tracker.MainActivity.BROADCAST_ACTION;
import static xyz.net7.android_home_14_fitness_tracker.MainActivity.MYSENSOR;


public class MyService extends Service implements SensorEventListener {

    SensorManager sensorManager;

    int dataCount = 0;
    int maxCount = 20;
    int DATA_COUNT_TO_SAVE=1;
    ArrayList<DataAcc> dataAccCurrentList;
    ArrayList<DataUserActivity> dataUserActivityListToSave = new ArrayList<DataUserActivity>();


    public static final String fileNameData = "accDataUser.txt";

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, " start  service", Toast.LENGTH_LONG).show();
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);

        dataAccCurrentList = new ArrayList<DataAcc>();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, " Service destroyed", Toast.LENGTH_LONG).show();
        if (sensorManager != null) {
            sensorManager.unregisterListener(this);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

                dataAccCurrentList.add(new DataAcc(event.values[0], event.values[1], event.values[2]));
                dataCount++;
                if (dataCount > maxCount) {
                    //get only maxCount values

                    if (checkUserActivity(dataAccCurrentList)) {
                        DataUserActivity dataUserActivity = new DataUserActivity();
                        dataUserActivity.setUserActivity(true);
                        dataUserActivityListToSave.add(dataUserActivity);

                    } else {
                        DataUserActivity dataUserActivity = new DataUserActivity();
                        dataUserActivity.setUserActivity(false);
                        dataUserActivityListToSave.add(dataUserActivity);
                    }

                    if (dataUserActivityListToSave.size() > DATA_COUNT_TO_SAVE) {
                        if (new File(fileNameData).exists()) {
                            //if file exist
                            appendDataToFile(dataUserActivityListToSave);
                        } else {//if file doesnt exist
                            writeDataToFile(dataUserActivityListToSave);
                        }
                    }

                    sendMyBroadcast(MYSENSOR, dataUserActivityListToSave.get(0), MyService.this);
                    dataCount = 0;
                    Toast.makeText(MyService.this, " Service is unregister listener", Toast.LENGTH_LONG).show();
                    sensorManager.unregisterListener(this);

                }
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public static void sendMyBroadcast(String sensor, DataUserActivity data, Context context) {

        Intent myIntent = new Intent(BROADCAST_ACTION);
        myIntent.putExtra(sensor, data);
        myIntent.setAction(BROADCAST_ACTION);
        context.sendBroadcast(myIntent);
    }

    public void writeDataToFile(ArrayList<DataUserActivity> dataList) {
        FileOutputStream fos = null;
        ObjectOutputStream outputStream = null;
        try {
            fos = openFileOutput(fileNameData, Context.MODE_PRIVATE);
            outputStream = new ObjectOutputStream(fos);
            outputStream.writeObject(dataList);
            Toast.makeText(MyService.this, " Write to file", Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            ex.printStackTrace();
            new AlertDialog.Builder(MyService.this)
                    .setTitle("Alert")
                    .setMessage(ex.toString())
                    .setCancelable(true)
                    .show();
        } finally {

            try {
                if (fos != null) fos.close();
                if (outputStream != null) outputStream.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }


    public void appendDataToFile(ArrayList<DataUserActivity> dataList) {
        FileOutputStream fos = null;
        AppendingObjectOutputStream appendingObjectOutputStream = null;

        try {
            fos = openFileOutput(fileNameData, Context.MODE_PRIVATE);
            appendingObjectOutputStream = new AppendingObjectOutputStream(fos);
            appendingObjectOutputStream.writeObject(dataList);
        } catch (Exception ex) {
            ex.printStackTrace();
            new AlertDialog.Builder(MyService.this)
                    .setTitle("Alert")
                    .setMessage(ex.toString())
                    .setCancelable(true)
                    .show();
        } finally {

            try {
                if (fos != null) fos.close();
                if (appendingObjectOutputStream != null) appendingObjectOutputStream.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public boolean checkUserActivity(ArrayList<DataAcc> dataAccList) {


        int listSize = dataAccList.size();
        float[] floatArrayX = new float[listSize];
        float[] floatArrayY = new float[listSize];
        float[] floatArrayZ = new float[listSize];

        for (int i = 0; i < dataAccList.size(); i++) {
            floatArrayX[i] = dataAccList.get(i).getAx();
            floatArrayY[i] = dataAccList.get(i).getAy();
            floatArrayZ[i] = dataAccList.get(i).getAz();
        }
//ascending numerical order
        Arrays.sort(floatArrayX);
        Arrays.sort(floatArrayY);
        Arrays.sort(floatArrayZ);

        if (floatArrayX[listSize - 1] - floatArrayX[0] >= 1) return true;
        if (floatArrayY[listSize - 1] - floatArrayY[0] >= 1) return true;
        if (floatArrayZ[listSize - 1] - floatArrayZ[0] >= 1) return true;

        return false;
    }


}