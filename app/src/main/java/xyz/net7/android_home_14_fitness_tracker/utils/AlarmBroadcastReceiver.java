package xyz.net7.android_home_14_fitness_tracker.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;
import android.widget.Toast;

/**
 * Created by user on 22.02.2018.
 */

public class AlarmBroadcastReceiver extends BroadcastReceiver

{
    public final long FIVE_MINUTES_INTERVAL = 1 * 60 * 1000;
    public final long FIFTEEN_MINUTES_INTERVAL = 15 * 60 * 1000;

    AlarmManager am;

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, " start Alarm service ", Toast.LENGTH_LONG).show();
        intent = new Intent(context, MyService.class);
        context.startService(intent);
    }

    public void setAlarm(Context context) {

        am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, AlarmBroadcastReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        //every 5 minutes
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            am.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(), FIVE_MINUTES_INTERVAL, pi);
            Toast.makeText(context, "Set Alarm  - set repeating!", Toast.LENGTH_LONG).show();
        }
        else{
        am.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,FIFTEEN_MINUTES_INTERVAL,pi);
            Toast.makeText(context, "Set Alarm  - set and allow while idle!", Toast.LENGTH_LONG).show();
        }

    }

    public void cancelAlarm(Context context) {
        Intent intent = new Intent(context, AlarmBroadcastReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        if (am != null) {
            am.cancel(sender);
            Toast.makeText(context, "Cancel Alarm !", Toast.LENGTH_LONG).show();
        }
    }
}
