package xyz.net7.android_home_14_fitness_tracker.model;


import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class DataAcc implements Serializable {
    float ax;
    float ay;
    float az;
    Date dataDate;

    public DataAcc(float ax, float ay, float az) {
        this.ax = ax;
        this.ay = ay;
        this.az = az;
    }

    public DataAcc() {
        this.ax = 0F;
        this.ay = 0F;
        this.az = 0F;
        this.dataDate = Calendar.getInstance().getTime();
    }

    public Date getDataDate() {
        return dataDate;
    }

    public void setDataDate(Date dataDate) {
        this.dataDate = dataDate;
    }

    public void setAx(float ax) {
        this.ax = ax;
    }

    public void setAy(float ay) {
        this.ay = ay;
    }

    public void setAz(float az) {
        this.az = az;
    }

    public float getAx() {
        return ax;
    }

    public float getAy() {
        return ay;
    }

    public float getAz() {
        return az;
    }

    @Override
    public String toString() {
        return "DataAcc{" +
                "ax=" + ax +
                ", ay=" + ay +
                ", az=" + az +
                ", dataDate=" + dataDate +
                '}';
    }
}