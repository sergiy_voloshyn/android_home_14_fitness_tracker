package xyz.net7.android_home_14_fitness_tracker.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;


public class DataUserActivity implements Serializable {
    public boolean userActivity;
    public Date userDate;


    public DataUserActivity(boolean userActivity, Date userDate) {
        this.userActivity = userActivity;
        this.userDate = userDate;

    }

    public DataUserActivity() {
        this.userActivity = false;
        this.userDate = Calendar.getInstance().getTime();

    }

    public void setUserActivity(boolean userActivity) {
        this.userActivity = userActivity;
    }

    public void setUserDate(Date userDate) {
        this.userDate = userDate;
    }

    public boolean getUserActivity() {
        return userActivity;
    }

    public Date getUserDate() {
        return userDate;
    }

    @Override
    public String toString() {
        return "DataUserActivity{" +
                "userActivity=" + userActivity +
                ", userDate=" + userDate +
                '}';
    }
}

