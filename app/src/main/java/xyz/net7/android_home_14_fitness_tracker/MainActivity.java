package xyz.net7.android_home_14_fitness_tracker;

/*

User story:
Вася работает курьером, бегает по утрам, ведёт активный образ жизни. Федя работает программистом и
любит в свободное время играть в компьютерные игры. Приложение должно определять образ жизни хозяина
 девайса. На экране должно быть два цвета: красный и зелёный. Чем активнее образ жизни человека -
 тем больше зелёного. Для Васи должно быть 90% зелёного, для Феди 90% красного.

 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import im.dacer.androidcharts.PieHelper;
import im.dacer.androidcharts.PieView;
import xyz.net7.android_home_14_fitness_tracker.model.DataAcc;
import xyz.net7.android_home_14_fitness_tracker.model.DataUserActivity;
import xyz.net7.android_home_14_fitness_tracker.utils.AlarmBroadcastReceiver;
import xyz.net7.android_home_14_fitness_tracker.utils.MyService;

import static xyz.net7.android_home_14_fitness_tracker.utils.MyService.fileNameData;

public class MainActivity extends AppCompatActivity {
    BroadcastReceiver broadcastReceiver;

    public final static String BROADCAST_ACTION = "xyz.net7.android_home_14_fitness_tracker.mybroadcast";
    public final static String MYSENSOR = "MYSENSOR";
    AlarmBroadcastReceiver alarmBroadcastReceiver;

    List<DataUserActivity> listData = null;
    @BindView(R.id.tv_date)
    TextView tvDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        alarmBroadcastReceiver = new AlarmBroadcastReceiver();

        Button startButton = (Button) findViewById(R.id.start_button);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alarmBroadcastReceiver.setAlarm(MainActivity.this);
            }
        });

        Button stopButton = (Button) findViewById(R.id.stop_button);
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alarmBroadcastReceiver.cancelAlarm(MainActivity.this);
            }
        });

        Button openButton = (Button) findViewById(R.id.open_file_button);
        openButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (readDataFile()) {
                    drawBars();
                }
            }
        });


        if (readDataFile()) {
            drawBars();
        }


        //my receiver
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                DataUserActivity data = (DataUserActivity) intent.getSerializableExtra(MYSENSOR);


                Date c = data.getUserDate();
                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy  HH:mm:ss");
                String formattedDate = df.format(c);
                tvDate.setText(formattedDate);
            }
        };
        IntentFilter myIntentFilter = new IntentFilter(BROADCAST_ACTION);
        registerReceiver(broadcastReceiver, myIntentFilter);


        Date c =Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy  HH:mm:ss");
        String formattedDate = df.format(c);
        tvDate.setText(formattedDate);

    }


    private boolean readDataFile() {
        FileInputStream fis = null;
        ObjectInputStream inputStream = null;

        listData = null;
        try {
            fis = openFileInput(fileNameData);
            inputStream = new ObjectInputStream(fis);
            listData = (List<DataUserActivity>) inputStream.readObject();

        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(this, ex.toString(), Toast.LENGTH_LONG).show();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                if (inputStream != null) inputStream.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        if (listData != null) {
            return true;
        }
        return false;
    }

    public void drawBars() {
        Date today = Calendar.getInstance().getTime();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String strDateToday = sdf.format(today);

        int userActive = 0;
        int userNotActive = 0;
        for (int i = 0; i < listData.size(); i++) {
            Date tempDate = listData.get(i).userDate;
            String strDateUser = sdf.format(tempDate);
            if (strDateUser.equals(strDateToday)) {
                if (listData.get(i).getUserActivity()) {
                    userActive++;
                } else {
                    userNotActive++;
                }
            }
        }

        float userActivePercent = (float) userActive / (float) (listData.size() / 100.0F);
        float userNotActivePercent = (float) userNotActive / (float) (listData.size() / 100.0F);

        PieView pieView = (PieView) findViewById(R.id.pie_view);
        ArrayList<PieHelper> pieHelperArrayList = new ArrayList<PieHelper>();

        PieHelper pieHelper1 = new PieHelper((int) userActivePercent, Color.RED);
        pieHelperArrayList.add(pieHelper1);

        PieHelper pieHelper2 = new PieHelper((int) userNotActivePercent, Color.GREEN);
        pieHelperArrayList.add(pieHelper2);

        pieView.setDate(pieHelperArrayList);
        //pieView.selectedPie(2); //optional
        pieView.showPercentLabel(true); //optional
    }
}

    /*    //sensor list
                -    /*    SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
       List<Sensor> listSensor = sensorManager.getSensorList(Sensor.TYPE_ALL);

       List<String> listSensorType = new ArrayList<>();
       for (int i = 0; i < listSensor.size(); i++) {
           listSensorType.add(listSensor.get(i).getName());*/


